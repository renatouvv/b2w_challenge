package br.com.b2wchallenge.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.client.MongoClients;

import br.com.b2wchallenge.model.Planet;
import br.com.b2wchallenge.service.PlanetService;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;

@DataMongoTest
@TestMethodOrder(OrderAnnotation.class)
@ComponentScan("br.com.b2wchallenge")
class PlanetControllerTests {

	private static MongodExecutable mongodExecutable;
	private static MongoTemplate mongoTemplate;
	
	@Autowired
	PlanetService service;
	
	@BeforeAll
	static void setUp() throws Exception {
		String ip = "localhost";
		int port = 27017;

		IMongodConfig mongodConfig = new MongodConfigBuilder().version(Version.Main.DEVELOPMENT)
				.net(new Net(ip, port, Network.localhostIsIPv6())).build();

		MongodStarter starter = MongodStarter.getDefaultInstance();
		mongodExecutable = starter.prepare(mongodConfig);
		mongodExecutable.start();
		mongoTemplate = new MongoTemplate(MongoClients.create("mongodb://localhost:27017"), "test");
	}

	@Test
	@Order(1)
	void testAddNewPlanetEmbedded() {
		Planet alderaan = new Planet();
		alderaan.setName("Alderaan");
		alderaan.setWeather("Sunny");
		alderaan.setTerrain("Tropical Forest");

		Planet tatooine = new Planet();
		tatooine.setName("Tatooine");
		tatooine.setWeather("Sunny");
		tatooine.setTerrain("Desert");

		mongoTemplate.save(alderaan, "collection");
		mongoTemplate.save(tatooine, "collection");

		assertEquals(2, mongoTemplate.findAll(Planet.class, "collection").size());
	}

	@Test
	@Order(2)
	void testAddNewPlanetAndGetAll() {
		Planet alderaan = new Planet();
		alderaan.setName("Alderaan");
		alderaan.setWeather("Sunny");
		alderaan.setTerrain("Tropical Forest");

		Planet tatooine = new Planet();
		tatooine.setName("Tatooine");
		tatooine.setWeather("Sunny");
		tatooine.setTerrain("Desert");

		service.addNewPlanet(alderaan);
		service.addNewPlanet(tatooine);

		assertEquals(2, service.getAllPlanets().size());
	}
	
	@Test
	@Order(3)
	void testGetPlanetByName() {
		Planet planet = service.getPlanetByName("Tatooine");
		assertEquals("Tatooine".toUpperCase(), planet.getName());
	}
	
	@Test
	@Order(4)
	void testDeletePlanetByName() {
		assertEquals(true, service.deletePlanetByName("Alderaan"));
		assertEquals(true, service.deletePlanetByName("Tatooine"));
	}
	
}