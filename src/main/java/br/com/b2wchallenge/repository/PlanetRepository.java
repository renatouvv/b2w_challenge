package br.com.b2wchallenge.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.b2wchallenge.model.Planet;

@Transactional
public interface PlanetRepository extends MongoRepository<Planet, Long> {
	
	public Planet findPlanetByName(String name);
	
	public Planet findPlanetById(String id);
}