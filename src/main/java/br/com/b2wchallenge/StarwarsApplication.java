package br.com.b2wchallenge;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.b2wchallenge.controller.PlanetController;

@SpringBootApplication
public class StarwarsApplication {
	
	@Bean
	ResourceConfig resourceConfig() {
		return new ResourceConfig().register(PlanetController.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(StarwarsApplication.class, args);
	}

}