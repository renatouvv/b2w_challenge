package br.com.b2wchallenge.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Component
public class ConnectionImpl implements Connection {

	@Override
	public JsonObject getConnection(String url) {
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet httpGet = new HttpGet("https://swapi.dev/api" + url);
		httpGet.addHeader("accept", "application/json");

		JsonObject jsonObject = null;
		
		try {
			HttpResponse response = httpClient.execute(httpGet);

			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader((response.getEntity().getContent())));

			String line;
			StringBuilder stringBuilder = new StringBuilder();
			while ((line = bufferedReader.readLine()) != null) {
				stringBuilder.append(line);
			}
			
			jsonObject = deserialize(stringBuilder.toString());
	        bufferedReader.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return jsonObject;
	}

	@Override
	public JsonObject deserialize(String json) {
		Gson gson = new Gson();
		JsonObject jsonClass = gson.fromJson(json, JsonObject.class);
		return jsonClass;
	}

}