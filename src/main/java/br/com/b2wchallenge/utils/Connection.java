package br.com.b2wchallenge.utils;

import com.google.gson.JsonObject;

public interface Connection {

	public JsonObject getConnection(String url);
	
	public JsonObject deserialize(String json);
	
}