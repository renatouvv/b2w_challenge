package br.com.b2wchallenge.model;

import java.util.ArrayList;

public class JsonPlanets {

	private float count;
	private String next;
	private String previous = null;
	ArrayList<JsonPlanet> results = new ArrayList<JsonPlanet>();

	// Getter Methods
	public float getCount() {
		return count;
	}

	public String getNext() {
		return next;
	}

	public String getPrevious() {
		return previous;
	}

	// Setter Methods

	public void setCount(float count) {
		this.count = count;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public void setPrevious(String previous) {
		this.previous = previous;
	}

	public ArrayList<JsonPlanet> getResults() {
		return results;
	}

	public void setResults(ArrayList<JsonPlanet> results) {
		this.results = results;
	}
	
}