package br.com.b2wchallenge.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.b2wchallenge.model.Planet;
import br.com.b2wchallenge.service.PlanetService;

@Controller
@Path("/starwars")
public class PlanetController {

	@Autowired
	PlanetService service;

	private static final Logger logger = LoggerFactory.getLogger(PlanetController.class);

	@Path("/planets")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<List<Planet>> getAll() {
		System.out.println("Entrou no método getAllPlanets");
		List<Planet> planets = service.getAllPlanets();
		return new ResponseEntity<List<Planet>>(planets, HttpStatus.OK);
	}

	@Path("/planet/add")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseEntity<Planet> add(String planetJson) {
		Planet planet;
		ResponseEntity<Planet> response;
		ObjectMapper mapper = new ObjectMapper();

		try {
			planet = mapper.readValue(planetJson, Planet.class);
			planet = service.addNewPlanet(planet);
			response = new ResponseEntity<Planet>(planet, HttpStatus.OK);
		} catch (Exception e) {
			response = new ResponseEntity<Planet>(new Planet(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error("Ocorreu um erro ao tentar adicionar o planeta." + "\n Mensagem: " + e.getMessage()
					+ "\n Causa: " + e.getCause());
		}

		return response;
	}

	@Path("/planet/find/id")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Planet> findById(@QueryParam(value = "id") String id) {
		Planet planet;
		ResponseEntity<Planet> response;
		try {
			planet = service.getPlanetById(id);
			response = new ResponseEntity<Planet>(planet, HttpStatus.OK);
		} catch (Exception e) {
			response = new ResponseEntity<Planet>(new Planet(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error("Ocorreu um erro ao consultar o planeta de id: " + id + "\n Mensagem: " + e.getMessage()
					+ "\n Causa: " + e.getCause());
		}
		return response;
	}

	@Path("/planet/find/name")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Planet> findByName(@QueryParam(value = "name") String name) {
		Planet planet;
		ResponseEntity<Planet> response;

		try {
			planet = service.getPlanetByName(name);
			response = new ResponseEntity<Planet>(planet, HttpStatus.OK);
		} catch (Exception e) {
			response = new ResponseEntity<Planet>(new Planet(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error("Ocorreu um erro ao consultar o planeta " + name + "\n Mensagem: " + e.getMessage()
					+ "\n Causa: " + e.getCause());
		}
		return response;
	}

	@Path("/planet/remove/id")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<String> deleteById(@QueryParam(value = "id") String id) {
		ResponseEntity<String> response;

		boolean planetExists = service.deletePlanetById(id);

		if (planetExists) {
			response = new ResponseEntity<String>("Planeta de id: " + id + " removido com sucesso.", HttpStatus.OK);
		} else {
			response = new ResponseEntity<String>("Não foi possível remover o planeta de id: " + id,
					HttpStatus.NO_CONTENT);
		}
		return response;
	}

	@Path("/planet/remove/name")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<String> deleteByName(@QueryParam(value = "name") String name) {
		ResponseEntity<String> response;

		boolean planetExists = service.deletePlanetByName(name);

		if (planetExists) {
			response = new ResponseEntity<String>("Planeta " + name + " removido com sucesso.", HttpStatus.OK);
		} else {
			response = new ResponseEntity<String>("Não foi possível remover o planeta " + name,
					HttpStatus.NO_CONTENT);
		}
		return response;
	}

}