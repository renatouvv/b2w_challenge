package br.com.b2wchallenge.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import br.com.b2wchallenge.model.JsonPlanet;
import br.com.b2wchallenge.model.JsonPlanets;
import br.com.b2wchallenge.model.Planet;
import br.com.b2wchallenge.repository.PlanetRepository;
import br.com.b2wchallenge.utils.Connection;

@Service
public class PlanetServiceImpl implements PlanetService {

	@Autowired
	PlanetRepository repository;
	
	@Autowired
	Connection conn;

	private static final Logger logger = LoggerFactory.getLogger(PlanetServiceImpl.class);
	
	public List<Planet> getAllPlanets() {
		List<Planet> planets = repository.findAll();
		for (Planet planet : planets) {
			planet = validateAndSetFilmsAppearance(planet);
		}
		return planets;
	}

	public Planet addNewPlanet(Planet planet) {
		planet.setName(planet.getName().toUpperCase());
		planet.setWeather(planet.getWeather().toUpperCase());
		planet.setTerrain(planet.getTerrain().toUpperCase());
		return repository.save(planet);
	}

	public Planet getPlanetById(String id) {
		Planet planet = repository.findPlanetById(id);
		return validateAndSetFilmsAppearance(planet);
	}

	public Planet getPlanetByName(String name) {
		Planet planet = repository.findPlanetByName(name.toUpperCase());
		return validateAndSetFilmsAppearance(planet);
	}

	public boolean deletePlanetById(String id) {
		boolean result;
		try {
			Planet planet = getPlanetById(id);
			repository.delete(planet);
			result = true;
		} catch (Exception e) {
			result = false;
			logger.info("Ocorreu um erro ao tentar remover o planeta de id " + id + "\n Mensagem: " + e.getMessage());
		}

		return result;
	}

	public boolean deletePlanetByName(String name) {
		boolean result;
		try {
			Planet planet = getPlanetByName(name.toUpperCase());
			repository.delete(planet);
			result = true;
		} catch (Exception e) {
			result = false;
			logger.info("Ocorreu um erro ao tentar remover o planeta " + name + "\n Mensagem: " + e.getMessage());
		}
		return result;
	}

	private int planetAppearanceCount(String name) {

		String url = "/planets";

		JsonObject jsonObject = conn.getConnection(url);
		
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		JsonPlanets planets = gson.fromJson(jsonObject, JsonPlanets.class);
		
		int filmsAppearance = 0;
		
		for (JsonPlanet planet : planets.getResults()) {
			if (planet.getName().equals(name)) {
				filmsAppearance = planet.getFilms().size();
				break;
			}
		}
		
		return filmsAppearance;
	}

	private Planet validateAndSetFilmsAppearance(Planet planet) {
		if (planet != null) {
			int appearance = planetAppearanceCount(planet.getName());
			planet.setMoviesShowed(appearance);
		}
		return planet;
	}

}