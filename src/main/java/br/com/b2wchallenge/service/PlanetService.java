package br.com.b2wchallenge.service;

import java.util.List;

import br.com.b2wchallenge.model.Planet;

public interface PlanetService {

	public List<Planet> getAllPlanets();

	public Planet addNewPlanet(Planet planet);

	public Planet getPlanetById(String id);

	public Planet getPlanetByName(String name);

	public boolean deletePlanetById(String id);

	public boolean deletePlanetByName(String name);

}
